# -*- coding: utf-8 -*-
import facebook
import requests
import json
import time

import logging
import FbTools

# Lanzar esto así:
# python web2py.py -S map_demo -M -R applications/map_demo/private/post_extractor.py



logger=logging.getLogger('web2py.app.map_demo')
logger.setLevel(logging.DEBUG)

POST_LIMIT = '100'
POST_QUERY = ("fields=id.as(post_id),created_time,message,full_picture,shares.as(shares_count),comments.limit(20).summary(true),"
         "reactions.type(NONE).limit(0).summary(total_count).as(reactions_none),"
         "reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),"
         "reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),"
         "reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),"
         "reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),"
         "reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),"
         "reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),"
         "reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)")


five_min = 5
twenty_min = 60
forty_min = 600
sleep_time = five_min

TOTAL_CPUTIME = 'total_cputime'
CALL_COUNT = 'call_count'
TOTAL_TIME = 'total_time'

KEEP_TRYING = 'keep trying same retailer'
JUMP_NEXT_RETAILER = 'jumping next retailer'
test_user = '119944138084029'

# access_token = 'EAACEdEose0cBALSAkcxo454FH1KiIiGmh88PFv9mTtBEqWEge8siaTU4YV7e83Q2CjiKN2dL4718duuFV1EB9zdHnZBJyNOe0Q95s500QGs9tRU8la2iG8vWAgicrmdkxb8ubcDFJsJvarh3QOoDE7AT7ceZCdbNDr55s21LP9SXB3XGGLATsnaSqUajV66y25zjcWyAZDZD'

import ast

class FacebookFeed:
    token_url = 'https://graph.facebook.com/oauth/access_token'
    params = dict(client_id='1786453524970622', client_secret='f5cb01876037ec0be0216e6d7806628f', grant_type='client_credentials')
# params_app2 = dict(client_id='1062939037171704', client_secret='3fef449411d75d0597dc9607acc2d05e', grant_type='client_credentials')

    token_generator = FbTools.TokenGenerator()
    headers = None
    retailers = 0

    def get_posts(self, user):
        self.retailers += 1
        try:
            # token APP:
            # token_response = requests.get(url=self.token_url, params=self.params)
            # access_token = token_response.json()['access_token']
            # token Páginas:
            # access_token = 'EAAPGvMe6fZCgBADPy8ZCrIoRviWpj0YZBRDH9fflBhJ9VMhNOh7LA7iCXc0KCFcQ2Gjoqxl2KGgrMxafbt3QZAPkRm56NuHRx537WU6zjerhj6ZCFGWJZChOD4ZBl7yt7OfMHMhd7krE6PhHwiBByWb9ZCrVbm2t0AwAyQrnCA6K8QZDZD'
            access_token = self.token_generator.getToken(self.headers)
            # logger.debug(access_token)
            # logger.debug(self.headers)
            graph = facebook.GraphAPI(access_token, version='2.12')

            profile = graph.get_object(user)
            # query_string = 'posts?'+ POST_QUERY +'&limit=' + POST_LIMIT + '&since=1496750843' #from 6 June 2017
            query_string = 'posts?'+ POST_QUERY +'&limit=' + POST_LIMIT + '&since=1528329600' #from 7 June 2018
            # query_string = 'posts?'+ POST_QUERY +'&limit=' + POST_LIMIT + '&since=1525608443' #from 6 May 2018
            posts = graph.get_connections(profile['id'], query_string)

            logger.debug('*****')
            logger.debug(str(len(posts['data'])))
            self.headers = posts["headers"]["x-app-usage"]
            logger.debug(posts["headers"]["x-app-usage"])
            sleep_time = five_min
            return posts
        except facebook.GraphAPIError as e:
            logger.debug(e)
            logger.debug('GENERATING NEXT TOKEN')
            self.token_generator.get_next_token()
            # if ('Application request limit reached' not in e.result['error']['message'] and 'Page request limit reached' not in e.result['error']['message'] ):
                # logger.debug(JUMP_NEXT_RETAILER)
                # return JUMP_NEXT_RETAILER
            # else:
                # logger.debug(KEEP_TRYING)
                # return KEEP_TRYING


def extract_posts(retailer, facebook_feeder):

    sleep_time = five_min

    user_fb_id = retailer.facebook_id

    logger.debug("Extracting posts for: "  + str(user_fb_id))

    fb_posts = facebook_feeder.get_posts(user=user_fb_id)

    # while (fb_posts == KEEP_TRYING):
        # logger.debug( "sleeping: " + str(sleep_time))
        # time.sleep(sleep_time)
        # fb_posts = FacebookFeed.get_posts(user=user_fb_id)

        # if (sleep_time == twenty_min):
            # sleep_time = forty_min
        # if (sleep_time == five_min):
            # sleep_time = twenty_min

    # if (fb_posts == JUMP_NEXT_RETAILER):
        # return

    if ((fb_posts!=None) and (fb_posts['data']!=None)):
        for fb_post in fb_posts['data']:

            p = transformFbPostJsonIntoDBJson(fb_post, user_fb_id)

            reactions = p.reactions_like + p.reactions_love + p.reactions_wow + p.reactions_haha + p.reactions_sad + p.reactions_angry + p.reactions_thankful
            if (retailer.fan_counts !=0):
                post_engagement = 100.0 * float(p.shares_count * 5 + len(p.comments) * 3 + reactions * 1) / retailer.fan_counts
            else:
                post_engagement = 0

            db.facebook_posts.insert(facebook_id=p.facebook_id,
                                    post_id = p.post_id,
                                    created_time = p.created_time,
                                    message = p.message,
                                    full_picture = p.full_picture,
                                    shares_count = p.shares_count,
                                    comments = p.comments,
                                    reactions_like = p.reactions_like,
                                    reactions_love = p.reactions_love,
                                    reactions_wow = p.reactions_wow,
                                    reactions_haha = p.reactions_haha,
                                    reactions_sad = p.reactions_sad,
                                    reactions_angry = p.reactions_angry,
                                    reactions_thankful = p.reactions_thankful,
                                    engagement = post_engagement,
                                    )
            db.commit()
    #TODO: log the error


def transformFbPostJsonIntoDBJson(fb_post, fb_id):
    post = Post()
    post.facebook_id = fb_id
    post.post_id = fb_post['post_id']
    post.created_time = fb_post['created_time']

    try:
        post.message = fb_post['message']
    except:
        pass

    try:
        post.full_picture = fb_post['full_picture']
    except:
        pass

    try:
        post.shares_count = fb_post['shares_count']['count']
    except:
        pass

    try:
        comments_array = (fb_post['comments']['data'])
        post.comments = ast.literal_eval(json.dumps(comments_array))
    except:
        pass

    post.reactions_like = fb_post['reactions_like']['summary']['total_count']
    post.reactions_love = fb_post['reactions_love']['summary']['total_count']
    post.reactions_wow = fb_post['reactions_wow']['summary']['total_count']
    post.reactions_haha = fb_post['reactions_haha']['summary']['total_count']
    post.reactions_sad = fb_post['reactions_sad']['summary']['total_count']
    post.reactions_angry = fb_post['reactions_angry']['summary']['total_count']
    post.reactions_thankful = fb_post['reactions_thankful']['summary']['total_count']
    return (post)

class Post:
 def __init__(self):
    self.facebook_id = 0
    self.post_id = 0
    self.created_time = 0
    self.message = 0
    self.full_picture = 0
    self.shares_count = 0
    self.comments = []
    self.reactions_like = 0
    self.reactions_love = 0
    self.reactions_wow = 0
    self.reactions_haha = 0
    self.reactions_sad = 0
    self.reactions_angry = 0
    self.reactions_thankful = 0

# retailers = db(db.retailers.fan_counts>15000).select()
# retailers = db((db.retailers.fan_counts<=15000) & (db.retailers.fan_counts >8000)).select()
# retailers = db((db.retailers.fan_counts>8000) & (db.retailers.waiting_posts_extraction == True)).select()
# retailers = db((db.retailers.posts_count == 0)).select()
retailers = db((db.retailers)).select()
facebook_feeder = FacebookFeed()

for retailer in retailers:
    extract_posts(retailer, facebook_feeder)
    retailer.update_record(waiting_posts_extraction = False)
    db.commit()
