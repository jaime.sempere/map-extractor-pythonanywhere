# -*- coding: utf-8 -*-
import facebook
import requests
import json
import time

import logging
import FbTools
from threading import Thread
from time import sleep
from random import choice


logger=logging.getLogger('web2py.app.map_demo')
logger.setLevel(logging.DEBUG)


POST_LIMIT = '100'
POST_QUERY = ("fields=id.as(post_id),created_time,message,full_picture,shares.as(shares_count),comments.limit(20).summary(true),"
         "reactions.type(NONE).limit(0).summary(total_count).as(reactions_none),"
         "reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),"
         "reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),"
         "reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),"
         "reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),"
         "reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),"
         "reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),"
         "reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)")


five_min = 5
twenty_min = 60
forty_min = 600
sleep_time = five_min

TOTAL_CPUTIME = 'total_cputime'
CALL_COUNT = 'call_count'
TOTAL_TIME = 'total_time'

KEEP_TRYING = 'keep trying same retailer'
JUMP_NEXT_RETAILER = 'jumping next retailer'
test_user = '119944138084029'

import ast

posts = []
class FacebookFeed:
    token_url = 'https://graph.facebook.com/oauth/access_token'
    base_url = 'https://graph.facebook.com/v3.0/'
    params = dict(client_id='1786453524970622', client_secret='f5cb01876037ec0be0216e6d7806628f', grant_type='client_credentials')
    # params = dict(client_id='1062939037171704', client_secret='3fef449411d75d0597dc9607acc2d05e', grant_type='client_credentials')

    token_generator = FbTools.TokenGenerator()
    headers = None
    retailers = 0

    def get_posts(self, user):
        self.retailers += 1
        try:
            access_token = self.token_generator.getToken(self.headers)
            graph = facebook.GraphAPI(access_token, version='3.0')

            profile = graph.get_object(user)
            query_string = 'posts?'+ POST_QUERY +'&limit=' + POST_LIMIT + '&since=1528329600' #from 7 June 2018
            posts = graph.get_connections(profile['id'], query_string)

            logger.debug('*****')
            logger.debug(str(len(posts['data'])))
            self.headers = posts["headers"]["x-app-usage"]
            logger.debug(posts["headers"]["x-app-usage"])
            sleep_time = five_min
            return posts
        except facebook.GraphAPIError as e:
            logger.debug(e)
            logger.debug('GENERATING NEXT TOKEN')
            self.token_generator.get_next_token()


def extract_posts(retailer, facebook_feeder):
    global posts

    sleep_time = five_min

    user_fb_id = retailer.facebook_id

    logger.debug("Extracting posts for: "  + str(user_fb_id))

    fb_posts = facebook_feeder.get_posts(user=user_fb_id)


    if ((fb_posts!=None) and (fb_posts['data']!=None)):
        for fb_post in fb_posts['data']:

            p = transformFbPostJsonIntoDBJson(fb_post, user_fb_id)

            reactions = p.reactions_like + p.reactions_love + p.reactions_wow + p.reactions_haha + p.reactions_sad + p.reactions_angry + p.reactions_thankful
            if (retailer.fan_counts !=0):
                post_engagement = 100.0 * float(p.shares_count * 5 + len(p.comments) * 3 + reactions * 1) / retailer.fan_counts
            else:
                post_engagement = 0

            p.engadgement = post_engagement
            posts.append(p)

def transformFbPostJsonIntoDBJson(fb_post, fb_id):
    post = Post()
    post.facebook_id = fb_id
    post.post_id = fb_post['post_id']
    post.created_time = fb_post['created_time']

    try:
        post.message = fb_post['message']
    except:
        pass

    try:
        post.full_picture = fb_post['full_picture']
    except:
        pass

    try:
        post.shares_count = fb_post['shares_count']['count']
    except:
        pass

    try:
        comments_array = (fb_post['comments']['data'])
        post.comments = ast.literal_eval(json.dumps(comments_array))
    except:
        pass

    post.reactions_like = fb_post['reactions_like']['summary']['total_count']
    post.reactions_love = fb_post['reactions_love']['summary']['total_count']
    post.reactions_wow = fb_post['reactions_wow']['summary']['total_count']
    post.reactions_haha = fb_post['reactions_haha']['summary']['total_count']
    post.reactions_sad = fb_post['reactions_sad']['summary']['total_count']
    post.reactions_angry = fb_post['reactions_angry']['summary']['total_count']
    post.reactions_thankful = fb_post['reactions_thankful']['summary']['total_count']
    return (post)

class Post:
 def __init__(self):
    self.facebook_id = 0
    self.post_id = 0
    self.created_time = 0
    self.message = 0
    self.full_picture = 0
    self.shares_count = 0
    self.comments = []
    self.reactions_like = 0
    self.reactions_love = 0
    self.reactions_wow = 0
    self.reactions_haha = 0
    self.reactions_sad = 0
    self.reactions_angry = 0
    self.reactions_thankful = 0
    self.engadgement = 0

retailers = db(db.retailers.waiting_posts_extraction == True).select()
print(len(retailers))
facebook_feeder = FacebookFeed()

# for idx, retailer in enumerate(retailers):
for idx in range(0, len(retailers), 3):

    subproceso1 = Thread(target=extract_posts, args=(retailers[idx], facebook_feeder,))
    subproceso1.start()
    subproceso2 = Thread(target=extract_posts, args=(retailers[idx + 1 ], facebook_feeder,))
    subproceso2.start()
    subproceso3 = Thread(target=extract_posts, args=(retailers[idx + 2 ], facebook_feeder,))
    subproceso3.start()
    subproceso4 = Thread(target=extract_posts, args=(retailers[idx + 3 ], facebook_feeder,))
    subproceso4.start()
    subproceso5 = Thread(target=extract_posts, args=(retailers[idx + 4 ], facebook_feeder,))
    subproceso5.start()
    subproceso6 = Thread(target=extract_posts, args=(retailers[idx + 5 ], facebook_feeder,))
    subproceso6.start()
    subproceso7 = Thread(target=extract_posts, args=(retailers[idx + 6 ], facebook_feeder,))
    subproceso7.start()
    subproceso8 = Thread(target=extract_posts, args=(retailers[idx + 7 ], facebook_feeder,))
    subproceso8.start()
    subproceso9 = Thread(target=extract_posts, args=(retailers[idx + 8 ], facebook_feeder,))
    subproceso9.start()
    subproceso10 = Thread(target=extract_posts, args=(retailers[idx + 9 ], facebook_feeder,))
    subproceso10.start()
    subproceso11 = Thread(target=extract_posts, args=(retailers[idx + 10 ], facebook_feeder,))
    subproceso11.start()
    subproceso12 = Thread(target=extract_posts, args=(retailers[idx + 11 ], facebook_feeder,))
    subproceso12.start()


    while( subproceso1.isAlive() or subproceso2.isAlive() or subproceso3.isAlive() or subproceso4.isAlive() or subproceso5.isAlive() or subproceso6.isAlive() or subproceso7.isAlive() or subproceso8.isAlive() or subproceso9.isAlive() or subproceso10.isAlive() or subproceso11.isAlive() or subproceso12.isAlive() ):
        print("Esperando...")
        sleep(0.5)

    for p in posts:
        db.facebook_posts.insert(facebook_id=p.facebook_id,
                                post_id = p.post_id,
                                created_time = p.created_time,
                                message = p.message,
                                full_picture = p.full_picture,
                                shares_count = p.shares_count,
                                comments = p.comments,
                                reactions_like = p.reactions_like,
                                reactions_love = p.reactions_love,
                                reactions_wow = p.reactions_wow,
                                reactions_haha = p.reactions_haha,
                                reactions_sad = p.reactions_sad,
                                reactions_angry = p.reactions_angry,
                                reactions_thankful = p.reactions_thankful,
                                engadgement = p.engadgement,
                                )
        db.commit()
    posts = []


    retailers[idx].update_record(waiting_posts_extraction = False)
    retailers[idx + 1].update_record(waiting_posts_extraction = False)
    retailers[idx + 2].update_record(waiting_posts_extraction = False)
    retailers[idx + 3].update_record(waiting_posts_extraction = False)
    retailers[idx + 4].update_record(waiting_posts_extraction = False)
    retailers[idx + 5].update_record(waiting_posts_extraction = False)
    retailers[idx + 6].update_record(waiting_posts_extraction = False)
    retailers[idx + 7].update_record(waiting_posts_extraction = False)
    retailers[idx + 8].update_record(waiting_posts_extraction = False)
    retailers[idx + 9].update_record(waiting_posts_extraction = False)
    retailers[idx + 10].update_record(waiting_posts_extraction = False)
    retailers[idx + 11].update_record(waiting_posts_extraction = False)

    print(idx)

    # extract_posts(retailer, facebook_feeder)
    db.commit()

