# -*- coding: utf-8 -*-
#################################
####    POST_EXTRACTION      ####
#################################
import facebook
import requests
import json
import time

import logging
import FbTools
from tornado.httpclient import AsyncHTTPClient
import tornado.ioloop


# from tornado.concurrent import Future
# from tornado.httpclient import AsyncHTTPClient

## Lanzar esto así:
# python web2py.py -S webextractor -M -R applications/webextractor/private/tornplaces.py


logger=logging.getLogger('web2py.app.webextractor')
logger.setLevel(logging.DEBUG)



test_user = '119944138084029'





token_url = 'https://graph.facebook.com/oauth/access_token'
params = dict(client_id='1786453524970622', client_secret='f5cb01876037ec0be0216e6d7806628f', grant_type='client_credentials')

token_generator = FbTools.TokenGenerator()
headers = None
retailers = 0

def build_url(latit, longit, token):
    radius = '177'
    api = 'v3.0'
    limit = '100'

    return "https://graph.facebook.com/" + api + "/search?q=&type=place&distance=" + radius + "&center=" + latit + "," +longit + "&fields=id,name,location,emails,fan_count,checkins,category,category_list,is_community_page,is_unclaimed,is_permanently_closed,about,description,website,phone,hours,link,price_range,payment_options,parking,public_transit,overall_star_rating,start_info,food_styles,restaurant_services,restaurant_specialties&limit=" + limit + "&access_token=" + token;

def treat_posts(response):
    logger.debug(response.body)
    try:
        logger.debug(response.headers["X-App-Usage"])
    except:
        logger.debug('no headers')
    pass

def asynchronous_fetch(url, callback):

    http_client = AsyncHTTPClient()

    def handle_response(response):
        callback(response)

    http_client.fetch(url, callback=handle_response)

def get_posts(latit,longit, callback):
    access_token = token_generator.getToken(headers)
    url = build_url(latit, longit, access_token)
    logger.debug(url)
    asynchronous_fetch(url, treat_posts)



indice = 0

get_posts('41.651802','-0.892856', treat_posts)


tornado.ioloop.IOLoop.instance().start()
