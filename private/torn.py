# -*- coding: utf-8 -*-
#################################
####    POST_EXTRACTION      ####
#################################
import facebook
import requests
import json
import time

import logging
import FbTools
from tornado.httpclient import AsyncHTTPClient
import tornado.ioloop


# from tornado.concurrent import Future
# from tornado.httpclient import AsyncHTTPClient

## Lanzar esto así:
# python web2py.py -S webextractor -M -R applications/webextractor/private/post_extractor.py


logger=logging.getLogger('web2py.app.webextractor')
logger.setLevel(logging.DEBUG)

POST_LIMIT = '100'
POST_QUERY = ("fields=id.as(post_id),created_time,message,full_picture,shares.as(shares_count),comments.limit(20).summary(true),"
         "reactions.type(NONE).limit(0).summary(total_count).as(reactions_none),"
         "reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),"
         "reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),"
         "reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),"
         "reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),"
         "reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),"
         "reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),"
         "reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)")


test_user = '119944138084029'

# access_token = 'EAACEdEose0cBALSAkcxo454FH1KiIiGmh88PFv9mTtBEqWEge8siaTU4YV7e83Q2CjiKN2dL4718duuFV1EB9zdHnZBJyNOe0Q95s500QGs9tRU8la2iG8vWAgicrmdkxb8ubcDFJsJvarh3QOoDE7AT7ceZCdbNDr55s21LP9SXB3XGGLATsnaSqUajV66y25zjcWyAZDZD'


sample_url = 'https://graph.facebook.com/v3.0/1030894873726085/posts?fields=id.as(post_id),created_time,message,full_picture,shares.as(shares_count),comments.limit(20).summary(true),reactions.type(none).limit(0).summary(total_count).as(reactions_none),reactions.type(like).limit(0).summary(total_count).as(reactions_like),reactions.type(love).limit(0).summary(total_count).as(reactions_love),reactions.type(wow).limit(0).summary(total_count).as(reactions_wow),reactions.type(haha).limit(0).summary(total_count).as(reactions_haha),reactions.type(sad).limit(0).summary(total_count).as(reactions_sad),reactions.type(angry).limit(0).summary(total_count).as(reactions_angry),reactions.type(thankful).limit(0).summary(total_count).as(reactions_thankful)&limit=100&since=1496750843&access_token=eaazayxrzcdfh4bam0tcvqsovv7nom1pajkh8narnkk2iumnwktdss17ddcqk8zmoegpl2taljpm3rreve2a8fzbigmwgskjm2sb7b1zb4eoqq6lmaeina9jv0zao0mqfx5ybr16yvjwseq1urytpza0sl6kvxakpz6iijun4vbsgzdzd'



token_url = 'https://graph.facebook.com/oauth/access_token'
params = dict(client_id='1786453524970622', client_secret='f5cb01876037ec0be0216e6d7806628f', grant_type='client_credentials')

token_generator = FbTools.TokenGenerator()
headers = None
retailers = 0

def build_url(fb_id, token):
    fb_id = str(fb_id)
    return 'https://graph.facebook.com/v3.0/1687413464864169?access_token=EAAZAYxRZCDfH4BAMEMexRFDEhxjW47fonD4TDJSw4K7YZBeJHdMvPOHZC5kGLU93m3A26cYspf2VHxb3Knmcs9jKhImB84l8jBxsXdIA9ZCgnKHzSvunHbEMBa2MaQrzocaxUwrVAZCdmMZAV6Xvzkx1tr9xZCGmNrbQC1gdlQZAJAAZDZD'
    return 'https://graph.facebook.com/v3.0/'+fb_id+'/posts?fields=id.as(post_id),created_time,message,full_picture,shares.as(shares_count),comments.limit(20).summary(true)&limit=100&since=1496750843&access_token=' + token


def treat_posts(response):
    try:
        logger.debug(response.headers["X-App-Usage"])
    except:
        logger.debug(response)
    pass
    logger.debug(response.body[1:400])
    with open('somefile.txt', 'a') as the_file:
        the_file.write('Hello\n')

def asynchronous_fetch(url, callback, fb_id):

    http_client = AsyncHTTPClient()

    def handle_response(response):
        callback(response)

    http_client.fetch(url, callback=handle_response)

def get_posts(fb_id, callback):
    logger.debug(fb_id)
    access_token = 'EAACEdEose0cBAHG8WeF9CMM4Y8Qq4T0TgDXfvneB1e4MLXHOaebnjh8rOjlDR2iKVCTbP9IVFqgi7hHq2p6QfCrQi1guWrN4kr06nKtKCGS3Ak0qCZAFAkXPwr2W82DR2DAxZA3uRBktTJIypSuJhiRoZA6sjopoKeF55gXqaOTtZBu6nsK7AGJrcTwCyoBTlzqJZCX0m9AZDZD'
    access_token = token_generator.getToken(headers)
    url = build_url(fb_id, access_token)
    asynchronous_fetch(url, treat_posts, fb_id)



# retailers = [ "1030894873726085", "743303112406704", "2102573200026120", "150579178466506", "611324132282537", "403284503158637", "267804516668129", "431121070297398", "697863923711199", "534127466973761", "216045205608139", "729004183834569", "505436733166104", "1596352517300445", "463091850512966", "540915246000370", "362637797105443", "1572986722798570", "618594218480972", "310717579130614"]


indice = 0

retailers = db((db.retailers)).select(limitby=(indice,(indice+1)*300))
logger.debug(len(retailers))

for retailer in retailers:
    get_posts(retailer.facebook_id, treat_posts)

tornado.ioloop.IOLoop.instance().start()
