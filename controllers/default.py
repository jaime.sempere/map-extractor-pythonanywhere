# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------
import os
import sys

import dateutil.parser

bussiness_sample = '206116006193007'

country_codes = {
  'AD':'Andorra', 'AE':'United Arab Emirates', 'AF':'Afghanistan', 'AG':'Antigua and Barbuda', 'AI':'Anguilla', 'AL':'Albania', 'AM':'Armenia', 'AN':'Netherlands Antilles', 'AO':'Angola', 'AQ':'Antarctica', 'AR':'Argentina', 'AS':'American Samoa', 'AT':'Austria', 'AU':'Australia', 'AW':'Aruba', 'AX':'Åland', 'AZ':'Azerbaijan', 'BA':'Bosnia and Herzegovina', 'BB':'Barbados', 'BD':'Bangladesh', 'BE':'Belgium', 'BF':'Burkina Faso', 'BG':'Bulgaria', 'BH':'Bahrain', 'BI':'Burundi', 'BJ':'Benin', 'BL':'Saint Barthélemy', 'BM':'Bermuda', 'BN':'Brunei', 'BO':'Bolivia', 'BQ':'Bonaire', 'BR':'Brazil', 'BS':'Bahamas', 'BT':'Bhutan', 'BV':'Bouvet Island', 'BW':'Botswana', 'BY':'Belarus', 'BZ':'Belize', 'CA':'Canada', 'CC':'Cocos [Keeling] Islands', 'CD':'Democratic Republic of the Congo', 'CF':'Central African Republic', 'CG':'Republic of the Congo', 'CH':'Switzerland', 'CI':'Ivory Coast', 'CK':'Cook Islands', 'CL':'Chile', 'CM':'Cameroon', 'CN':'China', 'CO':'Colombia', 'CR':'Costa Rica', 'CS':'Serbia and Montenegro', 'CU':'Cuba', 'CV':'Cape Verde', 'CW':'Curacao', 'CX':'Christmas Island', 'CY':'Cyprus', 'CZ':'Czechia', 'DE':'Germany', 'DJ':'Djibouti', 'DK':'Denmark', 'DM':'Dominica', 'DO':'Dominican Republic', 'DZ':'Algeria', 'EC':'Ecuador', 'EE':'Estonia', 'EG':'Egypt', 'EH':'Western Sahara', 'ER':'Eritrea', 'ES':'Spain', 'ET':'Ethiopia', 'FI':'Finland', 'FJ':'Fiji', 'FK':'Falkland Islands', 'FM':'Micronesia', 'FO':'Faroe Islands', 'FR':'France', 'GA':'Gabon', 'GB':'United Kingdom', 'GD':'Grenada', 'GE':'Georgia', 'GF':'French Guiana', 'GG':'Guernsey', 'GH':'Ghana', 'GI':'Gibraltar', 'GL':'Greenland', 'GM':'Gambia', 'GN':'Guinea', 'GP':'Guadeloupe', 'GQ':'Equatorial Guinea', 'GR':'Greece', 'GS':'South Georgia and the South Sandwich Islands', 'GT':'Guatemala', 'GU':'Guam', 'GW':'Guinea-Bissau', 'GY':'Guyana', 'HK':'Hong Kong', 'HM':'Heard Island and McDonald Islands', 'HN':'Honduras', 'HR':'Croatia', 'HT':'Haiti', 'HU':'Hungary', 'ID':'Indonesia', 'IE':'Ireland', 'IL':'Israel', 'IM':'Isle of Man', 'IN':'India', 'IO':'British Indian Ocean Territory', 'IQ':'Iraq', 'IR':'Iran', 'IS':'Iceland', 'IT':'Italy', 'JE':'Jersey', 'JM':'Jamaica', 'JO':'Jordan', 'JP':'Japan', 'KE':'Kenya', 'KG':'Kyrgyzstan', 'KH':'Cambodia', 'KI':'Kiribati', 'KM':'Comoros', 'KN':'Saint Kitts and Nevis', 'KP':'North Korea', 'KR':'South Korea', 'KW':'Kuwait', 'KY':'Cayman Islands', 'KZ':'Kazakhstan', 'LA':'Laos', 'LB':'Lebanon', 'LC':'Saint Lucia', 'LI':'Liechtenstein', 'LK':'Sri Lanka', 'LR':'Liberia', 'LS':'Lesotho', 'LT':'Lithuania', 'LU':'Luxembourg', 'LV':'Latvia', 'LY':'Libya', 'MA':'Morocco', 'MC':'Monaco', 'MD':'Moldova', 'ME':'Montenegro', 'MF':'Saint Martin', 'MG':'Madagascar', 'MH':'Marshall Islands', 'MK':'Macedonia', 'ML':'Mali', 'MM':'Myanmar [Burma]', 'MN':'Mongolia', 'MO':'Macao', 'MP':'Northern Mariana Islands', 'MQ':'Martinique', 'MR':'Mauritania', 'MS':'Montserrat', 'MT':'Malta', 'MU':'Mauritius', 'MV':'Maldives', 'MW':'Malawi', 'MX':'Mexico', 'MY':'Malaysia',
  'MZ':'Mozambique', 'NA':'Namibia', 'NC':'New Caledonia', 'NE':'Niger', 'NF':'Norfolk Island', 'NG':'Nigeria', 'NI':'Nicaragua', 'NL':'Netherlands', 'NO':'Norway', 'NP':'Nepal', 'NR':'Nauru', 'NU':'Niue', 'NZ':'New Zealand', 'OM':'Oman', 'PA':'Panama', 'PE':'Peru', 'PF':'French Polynesia', 'PG':'Papua New Guinea', 'PH':'Philippines', 'PK':'Pakistan', 'PL':'Poland', 'PM':'Saint Pierre and Miquelon', 'PN':'Pitcairn Islands', 'PR':'Puerto Rico', 'PS':'Palestine', 'PT':'Portugal', 'PW':'Palau', 'PY':'Paraguay', 'QA':'Qatar', 'RE':'Réunion', 'RO':'Romania', 'RS':'Serbia', 'RU':'Russia', 'RW':'Rwanda', 'SA':'Saudi Arabia', 'SB':'Solomon Islands', 'SC':'Seychelles', 'SD':'Sudan', 'SE':'Sweden', 'SG':'Singapore', 'SH':'Saint Helena', 'SI':'Slovenia', 'SJ':'Svalbard and Jan Mayen', 'SK':'Slovakia', 'SL':'Sierra Leone', 'SM':'San Marino', 'SN':'Senegal', 'SO':'Somalia', 'SR':'Suriname', 'SS':'South Sudan', 'ST':'São Tomé and Príncipe', 'SV':'El Salvador', 'SX':'Sint Maarten', 'SY':'Syria', 'SZ':'Swaziland', 'TC':'Turks and Caicos Islands', 'TD':'Chad', 'TF':'French Southern Territories', 'TG':'Togo', 'TH':'Thailand', 'TJ':'Tajikistan', 'TK':'Tokelau', 'TL':'East Timor', 'TM':'Turkmenistan', 'TN':'Tunisia', 'TO':'Tonga', 'TR':'Turkey', 'TT':'Trinidad and Tobago', 'TV':'Tuvalu', 'TW':'Taiwan', 'TZ':'Tanzania', 'UA':'Ukraine', 'UG':'Uganda', 'UM':'U.S. Minor Outlying Islands', 'US':'United States', 'UY':'Uruguay', 'UZ':'Uzbekistan', 'VA':'Vatican City', 'VC':'Saint Vincent and the Grenadines', 'VE':'Venezuela', 'VG':'British Virgin Islands', 'VI':'U.S. Virgin Islands', 'VN':'Vietnam', 'VU':'Vanuatu', 'WF':'Wallis and Futuna', 'WS':'Samoa', 'XK':'Kosovo', 'YE':'Yemen', 'YT':'Mayotte', 'ZA':'South Africa', 'ZM':'Zambia', 'ZW':'Zimbabwe'}

from pydal.objects import Row, Rows, Table, Query, Set, Expression

from map_logic import map_tools

POST_LIMIT = '100'
POST_QUERY = ("fields=id.as(post_id),created_time,message,full_picture,shares.as(shares_count),comments.limit(20).summary(true),"
         "reactions.type(NONE).limit(0).summary(total_count).as(reactions_none),"
         "reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),"
         "reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),"
         "reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),"
         "reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),"
         "reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),"
         "reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),"
         "reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)")

import ast


def changedb():
    city = request.args[0]
    redirect(URL('map', city))

#@cache(request.env.path_info, time_expire=1, cache_model=cache.ram)
#@cache(request.env.path_info, time_expire=24*60*60, cache_model=cache.ram)
def map():

    city = request.args[0]
    print(city)
    if city == 'zgz':
        response.cookies['city'] = 'ES'
    if city == 'sao':
        response.cookies['city'] = 'BR'
    print(response.cookies['city'])

    top_business = db(db.retailers).select()

    coordinates = ''
    for b in top_business:
        coordinates = coordinates + "long: " + str(b['longitude']) + " \n"
    import time
    return dict(points = top_business)
    t = time.ctime()
    # return dict(time=t, points = top_business, posts=get_posts())
    d = dict(time=t, points = top_business)
    return response.render(d)

def map_comparative():

    top_business = []

    import time
    t = time.ctime()
    # return dict(time=t, points = top_business, posts=get_posts())
    d = dict(time=t, points = top_business)
    return response.render(d)

def get_posts():
    facebook_id = (request.vars['facebook_id'])
    content = (request.vars['content'])
    posts = db(db.facebook_posts.facebook_id==facebook_id).select(orderby=~db.facebook_posts.created_time)
    if ((content != None) and (content !='')):
        terms = content.split(',')
        posts2 = db(db.facebook_posts.id<0).select()
        for term in terms:
            posts2 = posts2 & posts.find(lambda p: term.upper() in p.message_post.upper() )
        return posts2.as_json()

    return posts.as_json()

def get_posts_by_content():
    content = request.vars['content']
    posts = db(db.facebook_posts.contains(content)).select()

    return posts.as_json()

def get_retailers_by_posts_content():
    content = request.vars['content']
    engadgement = request.vars['engadgement']
    min_fans = int(request.vars['min_fans'])
    max_fans = int(request.vars['max_fans'])
    print max_fans
    vertical = (request.vars['vertical'])
    limitSearch = int(request.vars['limitSearch'])
    zip = request.vars['zip']

    if len(content)>0:
        #filter by content and maybe fans and engadgement
        retailers = filter_retailers_by_posts_content(content)
        if (min_fans!=0):
            retailers.exclude(lambda r: r.fan_count < min_fans)
        if (max_fans!=0):
            retailers.exclude(lambda r: r.fan_count > max_fans )
        if (engadgement!='0'):
            retailers.exclude(lambda r: engadgement > r.engadgement)
        if (zip!='0'):
            retailers.exclude(lambda r: zip != r.zip)
        if (len(vertical)>0):
            retailers = retailers.find(lambda r: r.vertical in (vertical) )
    else:
        retailers = db(db.retailers).select()
        print(len(retailers))
        if (min_fans!=0):
            retailers.exclude(lambda r: r.fan_count < min_fans)
        if (max_fans!=0):
            retailers.exclude(lambda r: r.fan_count > max_fans )
        if (engadgement!='0'):
            retailers.exclude(lambda r: engadgement > r.engadgement)
        if (len(vertical)>0):
            retailers = retailers.find(lambda r: r.vertical in (vertical) )
        if (zip!='0'):
            retailers.exclude(lambda r: zip != r.zip)

    return retailers.as_json()


def get_retailers_by_comparation():
    fan_count = (request.vars['fan_count'])
    sector = request.vars['sector']
    vertical = (request.vars['vertical'])
    cp = (request.vars['cp'])
    price_range = (request.vars['price_range'])

    retailers = get_all_retailers()
    print(len(retailers))

    if (fan_count!='None'):
        fan_count = int(fan_count)
        print('aplicando filtro fan_count')
        retailers.exclude(lambda r: r.fan_count > (1.4*int(fan_count)))
        retailers.exclude(lambda r: r.fan_count < (0.6*int(fan_count)))
    if (sector!='None'):
        retailers.exclude(lambda r: r.sector not in sector)
    if (price_range!='None'):
        retailers.exclude(lambda r: r.price_range != price_range)
    if (vertical!='None'):
        retailers.exclude(lambda r: r.vertical != vertical)
    if (cp!='None'):
        retailers.exclude(lambda r: r.zip != cp)

    print(len(retailers))
    return retailers.as_json()

def get_all_retailers():
    return db(db.retailers).select(orderby=~db.retailers.performance)

def get_retailers_by_name():
    retailer_name = request.vars['retailer_name']
    retailer = db(db.retailers.name.contains(retailer_name)).select()
    return retailer.as_json()


def filter_retailers_by_posts_content(contents):

    # facebook_ids = []
    # for (content in contents):
       # facebook_ids = facebook_ids + db(db.facebook_posts.message.contains(content)).select(db.facebook_posts.facebook_id))
    print("-" + contents + "-")
    contents = contents.split(',')

    facebook_ids = db(db.facebook_posts.message_post.contains(contents)).select(db.facebook_posts.facebook_id)

    fb_id_list = []
    for fb_id in facebook_ids:
        fb_id_list.append(fb_id.facebook_id)

    return db(db.retailers.facebook_id.belongs(fb_id_list)).select()

def filter_by_vertical_(vertical_string):
    vertical_string = vertical_string.replace(",", "|")
    vertical_string = vertical_string.replace("Leisure|", "Leisure,")
    verticals = vertical_string.split('|')
    if (len(verticals)>0):
        return db( db.retailers.vertical.belongs(verticals) ).select()
    else:
        return db(db.retailers.id>0).select()

def filter_by_engadgement_(engadgement, fans):
    return db( (db.retailers.engadgement >= engadgement) & (db.retailers.fan_count >= fans) ).select()


# index > preparingCSV.html > initialized.html > extracting.html

def index():
    redirect(URL('static', 'flashype/index.html'))
    #redirect(URL('map_comparative'))


# ---- API (example) -----
@auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET': raise HTTP(403)
    return response.json({'status':'success', 'email':auth.user.email})

# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def calculate_retailers_downing_logic():
    response.view = 'default/map.html'

    retailers = db(db.retailers.fan_count > FAN_COUNTS_THRESHOLD).select()
    points = []

    for ret in retailers:
      posts = db(db.facebook_posts.facebook_id == ret.facebook_id).select(limitby=(0,30))
      if len(posts)>1:
        prev_post = posts[0]
        total_freq = 0.0
        i = 0
        for post in posts:
          freq = (dateutil.parser.parse(prev_post.created_time) - dateutil.parser.parse(post.created_time)).days
          total_freq += freq
          prev_post = post
          i += 1
        total_freq = total_freq / (len(posts) - 1)
        if (total_freq > 1.2 * ret.posts_frequency):
            # print(ret.name)
            # print(ret.street)
            # print(ret.facebook_id)
            # print('*********')
            points.append(ret)
    return points

def calculate_retailers_downing():
    return dict(points=calculate_retailers_downing_logic())

def no_april_no_may_logic():
    # retailers = db(db.retailers).select()
    retailers = db(db.retailers.fan_count > FAN_COUNTS_THRESHOLD).select()
    points = []

    for ret in retailers:
      posts = db(db.facebook_posts.facebook_id == ret.facebook_id).select(limitby=(0,30))
      no_april_no_may = True
      if len(posts)>20:
        for post in posts:
          post_month = dateutil.parser.parse(post.created_time).month
          if ((post_month==5) or (post_month == 4)or (post_month == 6)):
              no_april_no_may = False
              break
        if (no_april_no_may):
            points.append(ret)
    print("no_april_no_may points: " + str(len(points)))
    return points

def no_april_no_may():
    response.view = 'default/map.html'


    return dict(points=no_april_no_may_logic())

def hints():
    response.view = 'default/map.html'

    no_april_may_june_facebook_ids = []
    for point in no_april_no_may_logic():
        no_april_may_june_facebook_ids.append(point.facebook_id)

    retailers_downing_facebook_ids = []

    for point in calculate_retailers_downing_logic():
        retailers_downing_facebook_ids.append(point.facebook_id)

    commons_ids = set(retailers_downing_facebook_ids).intersection(no_april_may_june_facebook_ids)
    print(commons_ids)

    matches = db(db.retailers.facebook_id.belongs(commons_ids)).select()
    print("matches is: " + str(len(matches)))

    return dict(points=matches)
