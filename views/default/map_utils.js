


<script>

  global_var_content = '';


  function bind_open_posts(){
      $( ".open_posts" ).click(function() {
        var fb_id = $(this).attr('fb_id');
        get_posts(fb_id);
      });
  }

  window.data_chart = [];
  function get_posts(fb_id){
    $.get(
      "/get_posts",
      "facebook_id=" + fb_id + "&content=" + global_var_content,
      function(posts) { 
          $('.modal-body').html('');
          var jsonPostsArray = $.parseJSON('[' + posts + ']')[0];

          var data_array = [];
          var post_index = 0;
          jsonPostsArray.forEach(function(post) {
            data_array.push({x:post.created_time, y:round_engagement(post.engadgement) });
            $('.modal-body').append(mount_post_content(post, post_index));
            post_index++;
          });

          $('#myModal').modal('toggle');
          draw_chart(data_array);
      },
      "html"
    );
  }


  function apply_clusters(){
      markerCluster = new MarkerClusterer(map, markers, {maxZoom: 17,
     zoomOnClick: false,
        imagePath: 'https://googlemaps.github.io/js-marker-clusterer/images/m'});
  }

  function round_engagement(engagement) {
    return Math.round(engagement*100)/100;
  }


</script>
