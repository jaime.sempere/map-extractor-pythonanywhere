# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# AppConfig configuration made easy. Look inside private/appconfig.ini
# Auth is for authenticaiton and access control
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.15.5":
    raise HTTP(500, "Requires web2py 2.15.5 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
configuration = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(configuration.get('db.uri'),
             pool_size=configuration.get('db.pool_size'),
             migrate_enabled=configuration.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = []
if request.is_local and not configuration.get('app.production'):
    response.generic_patterns.append('*')

# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = 'bootstrap4_inline'
response.form_label_separator = ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=configuration.get('host.names'))

# -------------------------------------------------------------------------
# create all tables needed by auth, maybe add a list of extra fields
# -------------------------------------------------------------------------
auth.settings.extra_fields['auth_user'] = []
auth.define_tables(username=False, signature=False)

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else configuration.get('smtp.server')
mail.settings.sender = configuration.get('smtp.sender')
mail.settings.login = configuration.get('smtp.login')
mail.settings.tls = configuration.get('smtp.tls') or False
mail.settings.ssl = configuration.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# read more at http://dev.w3.org/html5/markup/meta.name.html
# -------------------------------------------------------------------------
response.meta.author = configuration.get('app.author')
response.meta.description = configuration.get('app.description')
response.meta.keywords = configuration.get('app.keywords')
response.meta.generator = configuration.get('app.generator')

# -------------------------------------------------------------------------
# your http://google.com/analytics id
# -------------------------------------------------------------------------
response.google_analytics_id = configuration.get('google.analytics_id')

# -------------------------------------------------------------------------
# maybe use the scheduler
# -------------------------------------------------------------------------
if configuration.get('scheduler.enabled'):
    from gluon.scheduler import Scheduler
    scheduler = Scheduler(db, heartbeat=configure.get('heartbeat'))

db.define_table('retailers',
                Field('facebook_id', 'integer'),
                Field('name', 'string'),
                Field('emails', 'string'),
                Field('street', 'string'),
                Field('zip', 'string'),
                Field('city', 'string'),
                Field('country', 'string'),
                Field('latitude', 'string'),
                Field('longitude', 'string'),
                Field('fan_count', 'integer'),
                Field('checkins', 'string'),
                Field('category', 'string'),
                Field('category_1_id', 'string'),
                Field('category_1_name', 'string'),
                Field('category_2_id', 'string'),
                Field('category_2_name', 'string'),
                Field('category_3_id', 'string'),
                Field('category_3_name', 'string'),
                Field('category_4_id', 'string'),
                Field('category_4_name', 'string'),
                Field('category_5_id', 'string'),
                Field('category_5_name', 'string'),
                Field('is_community_page', 'string'),
                Field('is_unclaimed', 'string'),
                Field('is_permanently_closed', 'string'),
                Field('is_owned', 'string'),
                Field('about', 'string'),
                Field('description', 'string'),
                Field('website', 'string'),
                Field('phone', 'string'),
                Field('monday_hours', 'string'),
                Field('tuesday_hours', 'string'),
                Field('wednesday_hours', 'string'),
                Field('thursday_hours', 'string'),
                Field('friday_hours', 'string'),
                Field('saturday_hours', 'string'),
                Field('sunday_hours', 'string'),
                Field('link', 'string'),
                Field('price_range', 'string'),
                Field('accepts_american_express', 'string'),
                Field('accepts_cash_only', 'string'),
                Field('accepts_discover', 'string'),
                Field('accepts_mastercard', 'string'),
                Field('accepts_visa', 'string'),
                Field('has_lot_parking', 'string'),
                Field('has_street_parking', 'string'),
                Field('has_valet_parking', 'string'),
                Field('public_transit', 'string'),
                Field('overall_star_rating', 'string'),
                Field('start_type', 'string'),
                Field('start_date_year', 'string'),
                Field('start_date_month', 'string'),
                Field('start_date_day', 'string'),
                Field('food_styles', 'string'),
                Field('catering_service', 'string'),
                Field('delivery_service', 'string'),
                Field('group_friendly', 'string'),
                Field('kids', 'string'),
                Field('outdoor_seating', 'string'),
                Field('pickup_service', 'string'),
                Field('takes_reservations', 'string'),
                Field('takeout_service', 'string'),
                Field('has_waiters', 'string'),
                Field('welcomes_walkins', 'string'),
                Field('serves_breakfast', 'string'),
                Field('serves_coffe', 'string'),
                Field('serves_dinner', 'string'),
                Field('serves_drinks', 'string'),
                Field('serves_lunch', 'string'),
                Field('band_members', 'string'),
                Field('country_page_likes', 'string'),
                Field('best_page_id', 'string'),
                Field('best_page_name', 'string'),
                Field('parent_page_id', 'string'),
                Field('parent_page_name', 'string'),
                Field('culinary_team', 'string'),
                Field('general_manager', 'string'),
                Field('attire', 'string'),
                Field('messenger_ads_default_icebreakers', 'string'),
                Field('workflows', 'string'),
                Field('company_overview', 'string'),
                Field('general_info', 'string'),
                Field('founded', 'string'),
                Field('mission', 'string'),
                Field('birthday', 'string'),
                Field('place_topics', 'string'),
                Field('cover', 'string'),
                Field('place_type', 'string'),
                Field('can_checkin', 'string'),
                Field('can_post', 'string'),
                Field('displayed_message_response_time', 'string'),
                Field('engagement', 'string'),
                Field('global_brand_page_name'),
                Field('has_added_app', 'string'),
                Field('has_whatsapp_number', 'string'),
                Field('is_chain', 'string'),
                Field('is_always_open', 'string'),
                Field('is_messenger_platform_bot', 'string'),
                Field('is_published', 'string'),
                Field('rating_count', 'string'),
                Field('were_here_count', 'string'),
                Field('sector', 'string'),
                Field('vertical', 'string'),
                Field('posts_count', 'integer'),
                Field('verification_status', 'string'),
                Field('talking_about_count', 'string')
)


# Field('sector','string'),
            # Field('vertical','string'),
            # Field('engadgement','double'),
            # Field('posts_count','integer'),
            # Field('frequency','double'),
            # Field('posts_frequency','double'),
            # Field('performance','double'),
            # Field('waiting_posts_extraction','boolean', True)

db.define_table('facebook_posts',
        Field('facebook_id','string'),
        Field('post_id','string'),
        Field('created_time','string'),
        Field('message_post','string'),
        Field('full_picture','string'),
        Field('shares_count','string'),
        Field('comments','string'),
        Field('reactions_like','string'),
        Field('reactions_love','string'),
        Field('reactions_wow','string'),
        Field('reactions_haha','string'),
        Field('reactions_sad','string'),
        Field('reactions_angry','string'),
        Field('reactions_thankful','string'),
        Field('engadgement','double', default=0),
    )


# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)
