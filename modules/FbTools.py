# -*- coding: utf-8 -*-


from pprint import pprint

# token long live flashbot con punto :
my_pages = 'https://graph.facebook.com/v3.0/me/accounts?access_token=EAAZAYxRZCDfH4BAMEMexRFDEhxjW47fonD4TDJSw4K7YZBeJHdMvPOHZC5kGLU93m3A26cYspf2VHxb3Knmcs9jKhImB84l8jBxsXdIA9ZCgnKHzSvunHbEMBa2MaQrzocaxUwrVAZCdmMZAV6Xvzkx1tr9xZCGmNrbQC1gdlQZAJAAZDZD'

# token flashbot sin punto:
# my_pages = 'https://graph.facebook.com/v3.0/me/accounts?access_token=EAAPGvMe6fZCgBAEZAH9AboG7L9KgUxzR3v7sT5IXAJ3NykXudqGnSiznc5vZAictoXyhemUVwLdEq43000KZAibVr8pL8fT7TgfTZCCwBDT1KkXZCZA4KitapM3qAsgU3YGCKOi2r4yE5dBjYbU4lZBe4rUFcE5ZBDucGH3uHTN29NQZDZD'

token_flashbot_sinpunto = 'EAAPGvMe6fZCgBAEZAH9AboG7L9KgUxzR3v7sT5IXAJ3NykXudqGnSiznc5vZAictoXyhemUVwLdEq43000KZAibVr8pL8fT7TgfTZCCwBDT1KkXZCZA4KitapM3qAsgU3YGCKOi2r4yE5dBjYbU4lZBe4rUFcE5ZBDucGH3uHTN29NQZDZD'
token_flashbot_punto = 'EAAZAYxRZCDfH4BAMEMexRFDEhxjW47fonD4TDJSw4K7YZBeJHdMvPOHZC5kGLU93m3A26cYspf2VHxb3Knmcs9jKhImB84l8jBxsXdIA9ZCgnKHzSvunHbEMBa2MaQrzocaxUwrVAZCdmMZAV6Xvzkx1tr9xZCGmNrbQC1gdlQZAJAAZDZD'

import requests
import facebook
import json

NUMBER_OF_PAGES = 17

class TokenGenerator:
    #dont access this variables from outside the class (I could add the _underscore, but that decrease readibility)
    token_index = 0
    tokens_generated = []

    def __init__(self):
        self.__generateTokensArray()

    def __getPagesInfo(self):
        pages = requests.get(url= my_pages)
        if self.is_rate_limit_close(pages.headers):
            print(json.loads(pages.headers['x-app-usage']))
            raise Exception('RATE close to be consumed')
        # pprint(pages.json())
        return pages.json()

    def __generateTokensArray(self):
        print('*********************** GENERATING TOKENS')

        pages = self.__getPagesInfo()["data"]
        # pprint(pages)
        tokens = []

        for page in pages:
            tokens.append(page["access_token"])
        tokens.append(token_flashbot_punto)

        self.tokens_generated = tokens
        self.token_index = 0

    def getToken(self, headers_string = None):
        # print headers_string
        if (not headers_string):
            return self.get_current_token()

        if (headers_string):
            headers_json = json.loads(headers_string)
            if (self.is_rate_limit_close(headers_json)):
                return self.get_next_token()
            else:
                return self.get_current_token()

    def is_rate_limit_close(self,headers):
        try:
            app_usage = json.loads(headers['x-app-usage'])
            if (app_usage['call_count'] > 90):
                return True
            if (app_usage['total_cputime'] > 90):
                return True
            if (app_usage['total_time'] > 90):
                return True
            # print 'rate is ok'
        except:
            pass
        return False

    def get_next_token(self):
        print('============= GETTING NEXT TOKEN')
        # cambiando de token podemos sobrepasar las peticiones de facebook places:
        self.token_index += 1

        if (self.token_index >= len(self.tokens_generated)):
            # self.token_index = 0
            self.__generateTokensArray()

        return self.get_current_token()

    def get_current_token(self):
        return self.tokens_generated[self.token_index]


