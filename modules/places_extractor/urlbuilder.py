#!pyenv/versions/3.6.1/bin/python3.6

## This module, takes a urls.txt with latitude,longitude and build urls to feed graph api caller

def build_url(latit, longit):
    radius = '177'
    api = 'v3.0'
    limit = '1000'
    return "https://graph.facebook.com/" + api + "/search?q=&type=place&distance=" + radius + "&center=" + latit + "," +longit + "&fields=id,name,category_list&limit=" + limit;
    # return "https://graph.facebook.com/" + api + "/search?q=&type=place&distance=" + radius + "&center=" + latit + "," +longit + "&fields=id,name,location,emails,fan_count,checkins,category,category_list,is_community_page,is_unclaimed,is_permanently_closed,about,description,website,phone,hours,link,price_range,payment_options,parking,public_transit,overall_star_rating,start_info,food_styles,restaurant_services,restaurant_specialties&limit=" + limit;

with open('andorra_grid.csv','r') as grid_file:
    output = ''
    output_file = open('urls.txt', 'w+')
    for line in grid_file:
        if ('latitude' not in line):
            coord = line.replace(' ','').rstrip().split(',')
            output_file.write(build_url(coord[0], coord[1]))
            output_file.write('\n')

    output_file.close()
    grid_file.close()






