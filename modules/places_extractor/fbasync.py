#!pyenv/versions/3.6.1/bin/python3.6
import asyncio
from aiohttp import ClientSession
import json
import requests
from writeUtils import *
from myjson_utils import *
import sys
sys.path.append('/Users/jaimesempere/web2py/applications/webextractor/modules/')
import FbTools
import time
import re
import datetime as dt
#################################
##### CONFIGURATION PARAMS: #####
PARALEL_RATE = 2
SLEEP_BASE = 1
#################################

## This is the man array to process:
## We build urls from grid.csv files, and we use this array
##   to store the current status. We append/add next_pages links returned from Fb too
myurls_no_token = []

async def aync_calls(url, url_without_token,event_loop):
    async with ClientSession() as session:
        async with session.get(url) as response:

            if (is_rate_limit_close(response.headers)):
                print('rate limit!!!!!!!!!!')
                mark_need_next_token()

            response = await response.read()
            print(response[0:100])
            treat_response(response, url_without_token)

def treat_response(response, url_without_token):
    json_resp = convert_bytestring_to_json(response)

    if (not has_response_errors(json_resp)):
        if (is_not_empty(json_resp)):

            write_results_file(json_resp['data'])
            remove_url(url_without_token)
            append_next_page_if_present_to_my_urls(json_resp)
    else:
        write_errors_file(json_resp['error'])
        mark_need_next_token()

def append_next_page_if_present_to_my_urls(json_resp):
    global myurls_no_token
    try:
        next_page = get_next_page_url_if_present(json_resp)
        myurls_no_token.append(next_page)
        print('Next page appended:' + str(next_page))
    except:
        pass

def remove_url(url_without_token):
        print('REMOVING')
        global myurls_no_token
        myurls_no_token.remove(url_without_token)

def is_not_empty(response):
    if (len(str(response))>2):
        return True
    return False

def mark_need_next_token():
    global need_next_token
    need_next_token = True

def write_pending_urls_file(myurls):
    with open('urls.txt', 'w') as file_handler:
        for url in myurls:
            file_handler.write("{}\n".format(url))


myurls_no_token = load_urls_list_in_memory('urls.txt')
token_generator = FbTools.TokenGenerator()
token = token_generator.getToken()

print(len(myurls_no_token))
time.sleep(1)

inicio = dt.datetime.now()
event_loop = asyncio.get_event_loop()
futures = []

need_next_token = False
sleep_time = SLEEP_BASE

while (len(myurls_no_token)>0):

    index = 0

    while (index < len(myurls_no_token)):

        for i in range(0,PARALEL_RATE):
            if (index < len(myurls_no_token)):
                url = add_token_to_url(myurls_no_token[index],token)
                print(url)
                futures.append( asyncio.ensure_future (aync_calls(url, myurls_no_token[index], event_loop ) ))
                index += 1
        event_loop.run_until_complete(asyncio.wait(futures))

        if (need_next_token):
            token = token_generator.get_next_token()
            if (sleep_time < 3600):
                sleep_time *= 6
        else:
            if (sleep_time > SLEEP_BASE):
                sleep_time /= 6
        need_next_token = False

        print('SLEEP TIME is: ' + str(sleep_time) + ' - URLS to process: ' + str(len(myurls_no_token)))

        write_pending_urls_file(myurls_no_token)
        time.sleep(sleep_time)

    print(len(myurls_no_token))

fin = dt.datetime.now()
tiempo_procesado = (fin - inicio ).total_seconds()

def sayhello():
    return "hello"

print('Tiempo total procesado: ' + str(tiempo_procesado))
