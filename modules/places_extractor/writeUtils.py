#!pyenv/versions/3.6.1/bin/python3.6

RESULTS_FILE = 'results/results.json'
NEXT_PAGES_FILE = 'results/next_pages.txt'
ERRORS_FILE = 'results/errors.json'
URLS_PENDING_TO_PROCESS = 'results/urls_no_processed.txt'

import re

def remove_access_token(url):
    url_no_token = re.sub('(access.*?)\?','?', str(json_resp))
    return url_no_token

def get_next_page_url_if_present(json_resp):
    json_resp = json_resp['paging']['next']
    return remove_access_token(json_resp)

def write_results_file(json_resp):
    for place in json_resp:
        write_to_file(place,RESULTS_FILE)

def write_errors_file(json_resp):
    print('errors!!!!!!!!!!')
    write_to_file(json_resp,ERRORS_FILE)

def write_to_file(json_resp, filename):
    with open(filename, 'a') as the_file:
        the_file.write(str(json_resp) + '\n')

def load_urls_list_in_memory(file_name):
    myurls = []
    with open(file_name,'r') as urls_file:
        for line in urls_file:
            myurls.append(line.rstrip())
    return myurls

def add_token_to_url(url, token):
    return url + "&access_token=" + token

