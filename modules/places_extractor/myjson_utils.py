#!pyenv/versions/3.6.1/bin/python3.6
import json

def has_response_errors(json_resp):
    if 'error' in json_resp:
        print('*** ERROR *** ')
        print(json_resp.get('error'))
        return True
    else:
        return False

def build_url(latit, longit, token):
    radius = '177'
    api = 'v3.0'
    limit = '100'
    return "https://graph.facebook.com/" + api + "/search?q=&type=place&distance=" + radius + "&center=" + latit + "," +longit + "&fields=id,name,location,emails,fan_count,checkins,category,category_list,is_community_page,is_unclaimed,is_permanently_closed,about,description,website,phone,hours,link,price_range,payment_options,parking,public_transit,overall_star_rating,start_info,food_styles,restaurant_services,restaurant_specialties&limit=" + limit + "&access_token=" + token;

def convert_bytestring_to_json(byte_string):
    return json.loads(str(byte_string, 'utf-8'))

def is_rate_limit_close(headers):

    try:
        app_usage = json.loads(headers['x-app-usage'])
        print(app_usage)
        if (app_usage['call_count'] > 80):
            print('app calls: ' + str(app_usage['call_count']))
            return True
        if (app_usage['total_cputime'] > 80):
            print('total cputime: ' + str(app_usage['total_cputime']))
            return True
        if (app_usage['total_time'] > 80):
            print('total time: ' + str(app_usage['total_time']))
            return True
    except:
        pass

    try:
        page_usage = json.loads(headers['x-page-usage'])
        print(page_usage)
        if (page_usage['call_count'] > 80):
            print('page call_count: ' + str(page_usage['call_count']))
            return True
        if (page_usage['total_cputime'] > 80):
            print('page total_cputime: ' + str(page_usage['total_cputime']))
            return True
        if (page_usage['total_time'] > 80):
            print('page total_time: ' + str(page_usage['total_time']))
            return True
    except:
        pass

    return False
