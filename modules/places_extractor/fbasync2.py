#!/usr/local/bin/python3.5
#!/usr/local/bin/python3.5
import asyncio
from aiohttp import ClientSession

async def fetch(url, session):
    async with session.get(url) as response:
        return await response.read()

async def run(urls):
    tasks = []

    # Fetch all responses within one Client session,
    # keep connection alive for all requests.
    async with ClientSession() as session:
        for url in urls:
            task = asyncio.ensure_future(fetch(url, session))
            tasks.append(task)

        responses = await asyncio.gather(*tasks)
        # you now have all response bodies in this variable
        print(responses)

def print_responses(result):
    print(result)

urls = ['https://graph.facebook.com/v3.0/search?access_token=EAACEdEose0cBAOGqf1pYY0PHClZAhf200euzsAoq9kzwJb5oPIpaKooQalXQ0lerhQjAkZAWbz2OGcRdjZBRNnbdZB7OQX3TFpalyCCwlEIYnJmufsBkjRRwZAO580czUYMSYcSCL3hwK5LcAtUZAL4uQju6ccNA8H3TGokSQttph0mdax7sC2BDhkInMfk457z6oWJDZB5jQZDZD&pretty=0&fields=name%2Ccheckins&q=cafe&type=place&center=40.7304%2C-73.9921&distance=1000&limit=2&after=NwZDZD']
loop = asyncio.get_event_loop()
future = asyncio.ensure_future(run(urls))
loop.run_until_complete(future)
