
def get_posts():
    facebook_id = (request.vars['facebook_id'])
    content = (request.vars['content'])
    posts = db(db.facebook_posts.facebook_id==facebook_id).select()
    if ((content != None) and (content !='')):
        terms = content.split(',')
        posts2 = db(db.facebook_posts.id<0).select()
        for term in terms:
            posts2 = posts2 & posts.find(lambda p: term.upper() in p.message.upper() )
        return posts2.as_json()

    return posts.as_json()

